#! /usr/bin/python3
import argparse
from PhabRevAccBot import ReviewAcceptBot


def _parse_args():
    parser = argparse.ArgumentParser(description="Phabricator Bot which is used to accept revisions only if there is a specified number of Positiv Reviews")
    parser.add_argument('config', type=ReviewAcceptBot, help='path to configfile in json')
    return parser.parse_args(), parser


def main():
    c_args, parser = _parse_args()
    bot = c_args.config
    # bot._printlocals() # debugging
    bot.testconnection()
    bot.work()


if __name__ == "__main__":
    main()
