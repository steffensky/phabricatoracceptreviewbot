from enum import Enum

import json
import requests
import time
import datetime
import mysql.connector

_author_ = "Joshua Steffensky (joshua.steffensky@t-online.de"

_BASEURL_CONFIGKEY_ = "SERVERURL"
_USEHTTPS_CONFIGKEY_ = "HTTPS"
_BOTAPITOKEN_CONFIGKEY_ = "BOTAPITOKEN"
_NEEDEDREVIEWS_CONFIGKEY_ = "NEEDEDREVIEWS"
_CHECKFREQUENCY_CONFIGKEY_ = "CHECKFREQUENCY"
_MYSQL_HOST_CONFIGKEY_ =  "MYSQL_HOST"
_MYSQL_DATABASE_CONFIGKEY_ = "MYSQL_DATABASE"
_MYSQL_USER_CONFIGKEY_ =  "MYSQL_USER"
_MYSQL_PASSWORD_CONFIGKEY_ =  "MYSQL_PASSWORD"

_FORMAT_URLSTRING_ = "http{https}://{baseurl}/api/{command}"#

_PREPARED_STATEMENT_ = """ SELECT src, count(*) 
FROM edge INNER JOIN edgedata ON ID=dataID 
WHERE data LIKE '%accepted%' AND dst LIKE 'PHID-USER-%'AND NOT src  IN (select src from edge INNER JOIN edgedata WHERE data LIKE '%accepted%' and dst = %s) 
GROUP BY src HAVING count(*) >= %s
"""


class ReviewAcceptBot:
    def __init__(self, jsonconfigfile):
        with open(jsonconfigfile, "r") as configfile:
            config = json.load(configfile)
            self.__phabbaseurl = config[_BASEURL_CONFIGKEY_]
            self.__usehttps = config[_USEHTTPS_CONFIGKEY_]
            self.__apitoken = config[_BOTAPITOKEN_CONFIGKEY_]
            self.__neededreviews = config[_NEEDEDREVIEWS_CONFIGKEY_]
            self.__checkfrequency = config[_CHECKFREQUENCY_CONFIGKEY_]
            self.__mysql_host = config[_MYSQL_HOST_CONFIGKEY_]
            self.__mysql_database = config[_MYSQL_DATABASE_CONFIGKEY_]
            self.__mysql_user = config[_MYSQL_USER_CONFIGKEY_]
            self.__mysql_password = config[_MYSQL_PASSWORD_CONFIGKEY_]

        self.testconnection()
        self.setidentity()

    def _printlocals(self):
        print("BASEURL: ", self.__phabbaseurl)
        print("USEHTTPS: ", self.__usehttps)
        print("APITOKEN: ", self.__apitoken)
        print("NEEDED REVIEWS: ", self.__neededreviews)
        print("CHECKFREQUENCY: ", self.__checkfrequency)

    def work(self):
        while True:
            self.acceptifneeded()
            time.sleep(self.__checkfrequency)

    def testconnection(self):
        self.__pingserver()

    def setidentity(self):
        self.__identity = self.__dorequest(ConduitCommands.UserWhoAmI, {})

    def acceptifneeded(self):
        toaccept = self.__queryRevisions2Accept()
        for id in toaccept:
            print("[", datetime.datetime.now() , "] accepting revision: ", id)
            self.__acceptRevision(id)

    def __pingserver(self):
        params = {}
        self.__dorequest(ConduitCommands.ConduitPing, params)

    def __queryRevisions2Accept(self):
        acceptablePHIDs = self.__connecttodatabase()
       	acceptIDs = []
        if acceptablePHIDs :
        	revisions = self.__getOpenRevisionIDs(acceptablePHIDs)
        	for value in revisions:
            		acceptIDs.append(value["id"])
        return acceptIDs


    def __getOpenRevisionIDs(self, value):
        params = {}
        params["status"] = "status-open"
        params["phids[]"] = value
        print(params)
        return self.__dorequest(ConduitCommands.DifferentialQuery, params)

    def __acceptRevision(self, id):
        # print("called")
        params = {}
        params["revision_id"] = id
        params["action"] = "accept"
        params["silent"] = True
        self.__dorequest(ConduitCommands.DifferentialCreateComment, params)

    def __connecttodatabase(self):
        database = mysql.connector.connect(
            host=self.__mysql_host, 
            database= self.__mysql_database,
            user = self.__mysql_user, 
            password= self.__mysql_password)
        cursor = database.cursor(prepared=True)
        cursor.execute(_PREPARED_STATEMENT_, (self.__identity["phid"], self.__neededreviews))
        resultBytes =  cursor.fetchall()
        result = []
        for bytes in resultBytes:
            result.append(bytes[0].decode("utf-8"))
        return result

    def __dorequest(self, command,  payload):
        response = requests.get(self.__buildurl(command), params=self.__addtoken(payload)).json()
        print(command," " ,response)
        if response["error_code"] is not None:
            raise ConnectionError(response["error_info"])
        else:
            return response['result']

    def __addtoken(self, dict):
        dict["api.token"] = self.__apitoken
        return dict

    def __buildurl(self, command):
        return _FORMAT_URLSTRING_.format(https=('s' if self.__usehttps else ''), baseurl=self.__phabbaseurl, command=command.value)


class ConduitCommands(Enum):
    ConduitPing = "conduit.ping"
    DifferentialQuery = "differential.query"
    DifferentialCreateComment = "differential.createcomment"
    UserWhoAmI = "user.whoami"
